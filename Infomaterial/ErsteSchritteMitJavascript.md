Einstieg: Preisbruttorechner und Zinsrechner
============================================

JavaScript ist aus dem heutigen Web nicht mehr wegzudenken
und nimmt neben HTML und CSS eine enorm wichtige Rolle ein. Die Sprache
gibt es seit mehr als 10 Jahren und hat mittlerweile auch eine wachsende
Bedeutung über die Webprogrammierung hinaus gewonnen.

Sie sind unsicher, ob Sie JavaScript lernen möchten? Dann schauen Sie
doch mal hier nach:

<http://shouldilearnjavascript.com/>

Um Javascript kennen zu lernen wird im Unterricht das Programm Electron eingesetzt.
Electron verbindet die Grafische Oberfläche (index.html) mit der Logik (index.js) und auch der Datenbank (index.sql). Um das Zusammenwirken aller drei Ebenen der Programmierung beispielhaft kennen zu lernen, können Sie eine vorbereitete ReferenzApp clonen und ausprobieren.

>1. VSC starten
>2. `F1` drücken
>3. `git clone` eingeben
>4. `https://stbaeumer@bitbucket.org/stbaeumer/javascript.git` eingeben

Wenn Sie bisher dachten, dass ein Browser zur Javascript-Programmierung notwendig ist, dann liegen Sie richtig. Auch in Electron können Sie *-wie bei jedem Browser-* die Entwicklertools aufrufen. Es öffnen sich dann die Entwicklertools des integrierten Chrome-Browsers.

## Kurze Beschreibung des Inhalts des Projekts 

Folgende Dateien sind im geklonten Projekt enthalten, wobei nur die Dateien `index.html` und `index.js` für die Bearbeitung durch die Schüler vorgesehen sind:

* `index.html`-->  Definition aller Elemente der Oberfläche (Buttons, Textboxen, ...)
* `index.css` --> Gestaltung der Elemente aus der `index.html`
* `index.js` --> Logik des Programms 
* `index.sql` --> Datenbankprogramm namens `SqlLite`
* `README.md.` --> Kurze Info zum Arbeiten mit dieser Umgebung
* `Infometrial/ErsteSchritteInJavascript.md` --> Dieses Skript in digitaler Form

## Erste Ziele im Unterrichts

Javascript ist natürlich nicht nur im Zusammenhang mit Electron einsetzbar.

Für den Einstieg verfolgen wir zunächst diese Ziele:

-   **Kleine Berechnungen programmieren können**
-   **Ein- und Ausgabe auf der `index.html`.**

    -   Eingeben von Werten für eine Berechnung über ein HTML-INPUT-Feld

    -   Starten einer Berechnung mithilfe eines Buttons

    -   Ausgabe auf einem Label.

-   **Lernen gut wartbaren und gut lesbaren Code zu schreiben.**

    -   Kennen und Anwenden sinnvoller Programmierrichtlinien und
        Namenskonventionen

-    **Loggen auf der Konsole**

**Anmerkungen zur Konsole**

Eine Konsolen-API gibt es in jedem Browser. Electron hat hat den Chrome-Browser integriert, da er besonders schnell ist und auch neuere Standards bezüglich CSS und JavaScript anbietet.

> [**Console.alert()**](https://developer.mozilla.org/en-US/docs/Web/API/Console/assert)
>
> Meldung in einem modalen Fenster eingeben.
>
> [**Console.clear()**](https://developer.mozilla.org/en-US/docs/Web/API/Console/clear)
>
> Konsolenanzeige löschen..
>
> [**Console.log()**](https://developer.mozilla.org/en-US/docs/Web/API/Console/clear)
>
> Ausgabe auf der Konsole.

## Was genau steckt in Electron?

Electron (so wie es im Unterricht zum Einsatz kommt) ist ein fertig geschnürtes Paket, bestehend aus folgenden Komponenten:

 * `Chrome`-->  Browser 
 * `Node.js` --> Werkzeug zur serverseitigen Javascript-Programmierung. Out of the box kann Javascript den Browser (aus Sicherheitsgründen) nicht verlassen. Nur mit `node.js` ist es möglich mit Javascript die Grenzen des Browsers zu verlassen. `Node.js` ist modular aufgebaut. Das heißt, dass mit dem `Node Package Manager` (kurz `npm`) <https://www.npmjs.com/package/memory-stream> hunderte Module nachgeladen werden können, die jeweils eine bestimmte Funktionalität (z. B.  die Anbindung an eine Datenbank) mitbringen.  
 * `SQLite` --> Beliebtes Datenbanksystem zum dauerhaften Speichern von Eingaben. 


Der Bruttopreisrechner
======================

**Aufgabe: Das Programm liefert zu einem eingegebenen Nettopreis den
Bruttopreis.**

Für diese noch sehr übersichtliche Aufgabe benötigen wir schon einige
Grundstrukturen der Programmiersprache.

JavaScript-Programme verarbeiten Werte, die alle einen bestimmten
Datentyp haben.\
Die grundlegenden Datentypen sind zunächst:

-   Numbers (numerische Werte)

-   Strings (Zeichenketten): werden eingeschl0ssen in doppelte oder
    einzelne Anführungszeichen.
    **Vereinbarung** **(V1):**
    In JS verwenden wir die einfachen Anführungszeichen für Strings, da
    wir dann innerhalb die doppelten Anführungszeichen für Attributwerte
    für HTML verwenden dürfen.

-   Booleans (boolesche Werte): true (wahr) oder false (falsch).

Ganz wesentlich wird im Weiteren auch der Datentyp Objekt, der sich u.a.
auf Funktionen, Arrays, Datum/Zeit-Objekte sowie reguläre Ausdrücke
beziehen kann. Damit befassen wir uns nach und nach detaillierter.

Damit wir Werte auch speichern können, müssen wir sozusagen einen Platz
hierfür, eine **Variable**, im Programm anfordern und mitteilen, welcher
Wert dort zu speichern ist bzw. wie sich der gespeicherte Wert ändern
soll.

Das **Schlüsselwort** [^1]zum Einführen neuer Variablen ist **var** oder
**let**. Frühere JavaScript-Versionen kennen nur das Schlüsselwort var.
Werden außerhalb einer Funktion Variablen mit var eingeführt, sind sie
global (überall) bekannt und innerhalb eine Funktion nur in dieser
Funktion (functionscope, Scope bezeichnet den Sichtbarkeitsbereich einer
Variable). Mit dem Schlüsselwort let werden Variable auf den jeweiligen
Block beschränkt, in dem sie definiert werden (blockscope).

> let alter = 42;

Diese Anweisung bewirkt, dass eine Variable mit dem Namen alter angelegt
wird (**Deklaration**) und diese den Wert 42 direkt beim Anlegen erhält
(**Initialisierung**).

Bildhaft kann man sich den Speicher wie ein riesiges Schubladensystem
oder eine chaotische Lagerhaltung vorstellen. Mit dem Befehl let alter;
bzw. var alter; reservieren Sie eine solche Schublade bzw. einen solchen
Lagerplatz. Wo es genau gespeichert ist erfahren Sie nicht, aber wann
immer Sie den Inhalt der Schublade benötigen oder ändern wollen
verwenden Sie den vereinbarten Bezeichner und das System gibt ihnen den
Inhalt der Schublade oder legt auftragsgemäß einen (geänderten) Inhalt
dort ab.

**Achtung**: Links vom Zuweisungsoperator
steht immer der Lagerort und rechts der Inhalt, der gelagert werden
soll!

Wichtige grundsätzliche Regeln und Vereinbarungen:
--------------------------------------------------

-   **Vereinbarung** **(V2):**\
    JavaScript und EcmaScript erlauben teilweise auch undurchsichtige
    Anweisungen, die häufig zu unerwünschten Nebenwirkungen führen
    können. Um solchen Fallen aus dem Weg zu gehen, werden wir die
    Option **„use strict“;** verwenden. Die Option direkt zu Beginn gilt
    dann für das gesamte Programm. (Möglich wäre auch eine Verwendung
    nur in einzelnen Funktionen).

Der Befehl "use strict"; aktiviert eine strengere Interpretation. Dies
bedeutet im Einzelnen:

-   der Browser bricht bei einem Fehler die Ausführung ab

-   Variablen müssen deklariert werden

-   diverse zweifelhafte Konstrukte sind verboten

-   Vieles ist aus anderen Programmiersprachen vertraut: So z.B. die
    Zuweisung mit =, if-Anweisung um Fallunterscheidungen zu treffen,
    switch, for, while, do, try catch, // für Zeilenkommenare, /\*...\*/
    für mehrzeilige Kommentare Operatoren zum Rechnen (+, -, \* , /,
    Stringverkettung mit +)

-   Variablen sind dynamisch typisiert (d. h. mit der Zuweisung
    erschließt sich das Programm selbst den Typ, z. B. Verwendung von
    Anführungszeichen um den Wert.

    -   Zahlen sind immer Gleitkommazahlen,

    -   Objekterzeugung mit new, wird erst im Weiteren näher erläutert.

-   nicht zeilenorientiert. Daher kann es zu Problemen kommen, wenn das
    ; am Anweisungsende weggelassen wird.

-   **Vereinbarung** **(V3):**\
    In JS schließen wir jede Anweisungszeile mit einem Semikolon ab. Es
    würde zwar in vielen Fällen auch ohne korrekt funktionieren, man
    kauft sich aber sonst unerwarteterweise bei manchen Konstruktionen
    unerwünschte Nebenwirkungen ein!

-   Ebenfalls wie in vielen anderen Programmiersprachen sind für
    Bezeichner die meisten Sonderzeichen verboten, dürfen natürlich
    keinem Schlüsselwort entsprechen und dürfen nicht mit einer Ziffer
    beginnen. Erlaubt ist als Startzeichen der Unterstrich, das
    Dollar-Zeichen und jeder Buchstabe. Für eine gute Lesbarkeit treffen
    wir folgende Vereinbarung für den Gebrauch von Bezeichnern:\
    **Vereinbarung** **(V4):**\
    In JS bilden wir Bezeichner mit folgenden Regeln:

    -   Ein Bezeichner beginnt mit einem Kleinbuchstaben. Nur Klassenbezeichner beginnen mit einem Großbuchstaben.

    -   Wortteile werden durch die Camel-Schreibweise hervorgehoben.

    -   Wir beschränken uns i.d.R. auf Buchstaben des englischen
        Alphabets sowie Ziffern.

    -   Wir verzichten auf Sonderzeichen, insbesondere darf nie ein
        Leerzeichen in einem Bezeichner verwendet werden.

    -   In der Regel verwenden wir Nomen im Singular (z. B. person,
        rechnungsBetrag, …).\
        Für logische Werte (true/false) nutzen wir als Vorsilbe die
        Silbe is (englisch) oder ist.

**Sonderfall Konstanten**: Konstanten (Variable, deren Wert sich nicht
während der Programmlaufzeit ändern soll) werden mit dem Schlüsselwort
const eingeführt und wir wollen diese durchgängig mit Großbuchstaben
bezeichnen, Wortteile setzen wir mit Unterstrich ab (z. B. MWST_SATZ).

-   **Beispiele**:

Ganzzahlige Werte: var antwort = 42;

Gleitkommazahlen var gehalt = 1980.80;

const PI = 3.14;

Strings: var Gruss = ‘Hello World’;

Boolean: var isPrime = true;

-   **Beispiele für Schlüsselworte in JS sind :** null, true, false,
    undefined, NaN, var, let, const, …

Erstes Praxisbeispiel: Der Bruttorechner
----------------------------------------

Um zu einem Nettopreis einen Bruttopreis auszurechnen genügen wenige
Zeilen Code, die

-   Die Variablen nettoPreis und bruttoPreis einführen und
    initialisieren.

-   Die Konstante MWST_SATZ einführt.

-   Die Berechnung nettoPreis + nettopreis * MWST_SATZ oder mit
    `nettoPreis * (1 + MWST_SATZ)` durchführt und das Ergebnis in die
    „Schublade“ bruttoPreis legt.

-   Und schließlich die Ausgabe des Ergebnisses auf dem Label.

Die Leerzeichen vor und nach jedem Operator sind nicht notwendig,
erhöhen aber wieder die Lesbarkeit. Also mal wieder Zeit für eine
weitere Vereinbarung!

-   **Vereinbarung** **(V5):**
    In JS schreiben wir zur besseren Lesbarkeit vor und nach jedem
    Operator (z. B. beim =, +, \*, …) ein Leerzeichen!

-   **Vereinbarung** **(V6):**
    Die Benennung der Funktionen und Methoden ist konsistent durchzuführen. Eine  btnClick-Funktion, die die Aufgabe hat ein neues Auto einzutragen soll entsprechend 
    `btnAutoEintragenOnClick()` heißen und die Instanzmethode `autoEintragen()` aufrufen.

Wie lässt sich jetzt `index.html` mit `index.js` verbinden?

Dies gelingt mit Hilfe der DOM-API. Hier bei steht DOM für Document
Orientation Model und API für A Programming Interface. Dies ist ein
standardisierte Modell und eine Schnittstelle um Elemente in HTML
auszulesen, zu ändern, dynamisch hinzuzufügen oder zu löschen. Das HTML
DOM definiert in einer Baumstruktur alle HTML-Elemente als Objekte mit
zugehörigen Eigenschaften. Weiter stellt es Methoden für den Zugriff auf
alle HTML-Elemente sowie Standardereignisse (Events) für alle
HTML-Elemente.

Beispiel:
---------

Am Häufigsten wird die getElementById-Methode benannt. Hierzu muss in
HTML-Element mit einer eindeutigen ID ausgezeichnet sein. Im HTML-Dokument wird ein Element über eine ID eindeutig festgelegt. innerHTML kennzeichnet alles was zwischen dem Öffnen und dem Schließen-Tag des identifizierten Elements steht.

Unter [www.zinsrechner-online.com](http://www.zinsrechner-online.com)
verbirgt sich eine Seite, auf der man Zinsrechnungen für verschiedene
Anlageformen (Einmalsparen, Ratensparen, …) durchführen kann.

Testen Sie den Zinsrechner dort aus und versuchen an Hand einiger
Zahlenbeispiele die Rechnungen dort nachzuvollziehen.

# Info und Aufgabe Datenbank

Datenbanken kommen immer dann zu Einsatz, wenn Daten nach dem erneuten Starten eines Programms weiterhin zur Verfügung stehen sollen.

## Beispiele für den Einsatz von Datenbanken:

* Die Schulsekretärin legt einen neuen Schüler im Schulverwaltungsprogramm an. Für die Dauer des Schulbesuchs des Schülers werden seine Stammdaten gespeichert.
* Auf dem Handy werden nach jedem Spiel die Highscores gespeichert.
* Damit der Verlauf in WhatsApp erhalten bleibt, wird für jede neue Nachricht der Whatsapp-Datenbank ein neuer Datensatz hinzugefügt.

## SQL

Die Kommunikation des Javascript-Programms mit der Datenbank geschieht über die Datenbank-Sprache *SQL*. 
Die Anweisungen zum Einfügen, Auslesen usw. werden in SQL formuliert und dann als String an die Datenbank übergeben. 

Im Folgenden werden beispielhaft SQL-Anweisung gezeigt und besprochen. Die Anweisungen sind jeweils eingebettet in Javascript-Code zum Ausführen der SQL-Anweisungen.

### `CREATE`
Mit `Create` wird eine neue Tabelle in der Datenbank anlegt.
```javascript
db.run("CREATE TABLE IF NOT EXISTS autoTabelle (ps int, farbe char);");
```
Hier wird eine neue Tabelle namens autoTabelle mit zwei Spalten, nämlich ps und farbe angelegt. Anders als etwa in Excel muss beim Anlegen der Datentyp eines jeden Attributs festgelegt werden. 

### `INSERT`
Mit `INSERT` wird ein neuer Datensatz in eine Tabelle einfügt.

```javascript
auto.leistungInPs = 100;
auto.farbe = "grün";
db.run("INSERT INTO autoTabelle(ps, farbe) VALUES (?,?)", [auto.leistungInPs, auto.farbe]);
```

Hier wird ein Datensatz in die bestehende Tabelle `autoTabelle` eingefügt. Die Anzahl der Fragezeichen entspricht genau der Anzahl der Spalten aus der vorhergehenden `CREATE`-Anweisung. Jedes Fragezeichen ist Platzhalter für einen konkreten Wert aus den eckigen Klammern. Das erste Fragezeichen wird ersetzt durch den konkreten Wert der Eigenschaft `auto.leistungInPs`. Das zweite Fragezeichen wird ersetzt durch den konkreten Wert der Eigenschaft `auto.farbe`.

### `UPDATE`
Mit Update werden bestehende Datensätze aktualisiert.  

```javascript
auto.leistungInPs = 100;
auto.farbe = "grün";
auto.marke = "mercedes";
db.run("UPDATE autoTabelle SET leistugInPs = ?, farbe = ? WHERE marke = ?",[auto.leistungInPs, auto.farbe, auto.marke]);
```
Hier werden alle Datensätze aktualisiert, deren Marke dem Wert der Eigenschaft `auto.marke` entspricht. Konkret bekommen hier alle *Mercedes* eine grüne Farbe und 100 PS Leistung. 

### `DELETE`
Mit Delete werden Datensätze gelöscht.

```javascript
auto.marke = "mercedes";
db.run("DELETE FROM autoTabelle WHERE marke = ?", [auto.marke]);
```
Hier werden alle Datensätze gelöscht, deren Marke dem Wert der Variablen `marke` entspricht. Konkret werden alle *Mercedes* aus der Tabelle entfernt.

### `SELECT`
Mit `SELECT` wird die Tabelle *-anders als bei den vorhergeneden Operationen-* nicht verändert, sondern lediglich abgefragt.

```javascript
auto.farbe = "grün";
db.prepare("SELECT * FROM autoTabelle WHERE farbe = ?", [auto.farbe]);
```
Hier werden alle Eigenschaften (`*`) der Tabelle autoTabelle abgefragt, die die Bedingung (`WHERE`) erfüllen, dass die Farbe dem Wert der Eigenschaft `auto.farbe` entspricht.

## Begriffe

### Datentypen ##
Es werden u. a. folgende Datentypen unterschieden. 
|Name   | Typ   | Beispiel |
|-------|-------|-----|
|INT|Ganzzahl|Anzahl, AlterInJahren|
|REAL|Fließkommazahl|Währungsbetrag, NoteDezimal|
|TEXT|Zeichenkette|Name, Beschreibung|


### Datenbank
Die Datenbank trägt einen Namen. In einer relationalen Datenbank werden alle Sachverhalte in Tabellen abgespeichert. Jeder Sachverhalt bekommt seine eigene(n) Tabelle(n). 

>In einem Autoverwaltungsprogramm gibt es eine Tabelle für Autos, eine für Fahrer, eine für Führerscheine usw.

### Tabelle
Jede Tabelle ist unter Ihrem eindeutigen Namen ansprechbar. Jede Spalte (Attribut) trägt einen Namen und kann nur Werte eines bestimmten Typs aufnehmen. 

> `int` entspricht dem Typ Ganzzahl. Beispiel *LeistungInPs*

> `char` steht für Zeichenketten. Beispiel *Farbe*

### Datensatz (Zeile)
Jede Zeile in einer Tabelle entspricht einem Datensatz. Ein Datensatz erfasst also alle Daten eines Objekts der realen Welt.

> In der Tabelle namens *autoTabelle* im Autoverwaltungsprogramm werden die Eigenschaftswerte der verschiedenen Autos mit ihren relevanten Eigenschaften erfasst.

### Datenfeld
Ein Datenfeld ist die Schnittmenge einer einzelnen Spalte (Attribut) mit einer einzelnen Zeile (Datensatz). Eine Datenzelle enthält einen einzelnen Wert.

> Die Farbe *grün* ist der Wert an der Schnittmenge der Spalte *Farbe* und der ersten *Zeile*

## Datenbank-Anbieter
Die Auswahl an Datenbank-Anbietern / -Herstellern ist groß. Obwohl alle relationalen Datenbanken grundsätzlich *SQL*  sprechen, gibt es im Detail jedoch Unterschiede. 
Kriterien zur Auswahl des richtigen Anbieters könnten sein:

* Performance / Geschwindigkeit
* Kosten
* Datenschutz / Datensicherheit
* Mehrbenutzerfähigkeit

Im Falle einer besonders kleinen Umgebung (z.B. einer Electron-App) ist [SQLite](https://de.wikipedia.org/wiki/SQLite) eine gute Wahl. Der große Vorteil im Zusammenhang mit Javascript liegt darin, dass SQLite als reine Javascriptdatei in dieser ReferenzApp enthalten ist, ohne dass ein Programm installiert werden muss.

# Aufgabe Handyverwaltung

Für einen Handyshop sollen Sie eine Software zur Verwaltung von Smartphones erstellen. Es gilt, dass bei Klick auf einen entsprechenden Button ein neues Handy mit seinen relevanten Eigenschaften über Textboxen eingelesen wird. Gleichzeitig werden die Eigenschaftswerte des Handys auf dem Label ausgeben. 

Zusätzlich soll ein zweiter Button angelegt werden, der bei Klick *alle* Handys aus der Datenbank ausliest und auf dem Label zeilenweise anzeigt. 

**Tipp:** Machen Sie Gebrauch von `console.log()`, um auf der Konsole den Erfolg Ihrer Arbeit verfolgen zu können.

**Tipp:** Schalten Sie mit [ALT] + [Tab] zwischen VSC und App hin und her.

**Tipp:** Schalten Sie mit [Strg] + [Tab] innerhalb von VSC zwischen den geöffneten Dateien hin und her.

**Tipp:** Beschleunigen Sie Ihre Arbeit mit Suchen und Ersetzen ([STRG] + [F]).

### Planung des Prozesses ###

1. Gestalten Sie die graphische Oberfläche nach Anweisung.

2. Entwerfen Sie die Klasse, indem Sie von drei relevanten Eigenschaften des Objekts der realen Welt ausgehen. Die drei relevanten Eigenschaften der realen Welt müssen zu Eigenschaften einer sinnvoll bezeichneten Klasse werden.

3. Beim Starten des Programms muss eine neue Datenbank angelegt werden. Dazu müssen Sie der Datenbank einen sinnvollen Namen geben und die `CREATE`-Anweisung an die von Ihnen identifizierten konkreten Eigenschaften der realen Welt anpassen.

4. Beim Anlegen eines neuen Handys ist *-wie gewohnt-* ein neues Objekt zu instanziieren und initialisieren. Zusätzlich muss ein neuer Datensatz in der Datenbank angelegt werden. Eine `INSERT`-Anweisung zum Anlegen eines neuen Datensatzes ist in der `index.js` bereits vorhanden. Sie müssen also lediglich die vorhandene `INSERT`-Anweisung in die btnClick-Funktion verschieben. `CREATE`- und `INSERT`-Anweisung müssen in Anzahl und Reihenfolge der Attribute übereinstimmen. Die Datenbank muss anschließend auf die Festplatte geschrieben werden.

5. Bislang haben Sie nur ein Objekt auf dem Label ausgegeben. Da aber in der Datenbank jeder Datensatz ein eigenes Objekt repräsentiert und jede Datenbank viele Datensätze je Tabelle enthalten kann, sollen Sie laut Aufgabe alle Objekte gleichzeitig auf dem Label ausgegeben. Dazu müssen Sie die `SELECT`-Anweisung und die anschließende Schleife in die entsprechende Button-Click-Methode kopieren. In der `SELECT`-Anweisung muss lediglich der Name der Tabelle agepasst werden, da das `*` ohnehin alle Eigenschafteswerte abfragt. Innerhalb des Rumpfes der existierenden `while`-Schleife muss ein Ausgabe-String mit jedem Schleifendurchlauf um die Eigenschaftswerte des jeweiligen Datensatzes verlängert werden. Das `<br>`-HTML-Tag bewirkt einen Zeilenumbruch.
Dazu eine beispielhafte Überarbeitung der Schleife im Sinne der Aufgabe:
```
let ausgabe = "";

while ... 
{
...
ausgabe = ausgabe + row.meineEigenschaft1 + "," + row.meineEigenschaft2 + <br>
}
return ausgabe // Rückgabe auf das Label
```

# Aufgabe Erweiterung Handyverwaltung 

Erweitern Sie Ihre Handyverwaltung mit sinnvollen Abfragen! Beachten Sie die Hinweise:
* Jede weitere Abfrage bekommt einen eigenen Button mit sprechender Aufschrift.
* Erwünscht sind Abfragen zu den Operationen `SELECT`, `UPDATE`, `DELETE`. 
* Konsultieren Sie das Internet (Stichworte: *SQL, SQLite, Tutorial, Beispiele*), um die Syntax zu Ihren Ideen zu re­cher­chie­ren.
* Im Zusammenhang mit *SELECT* sollen Sie auch spaltenweise und zeilenweise (`WHERE`) filtern, sortieren (`ORDER BY ... DESC`), Filter verknüpfen (`AND`, `OR`), ...  







## Begriffe auseinanderhalten​

***Zum besseren Verständnis hier ein kleiner Lückentextzum zum Verständnis der Begriffe rund um die Datenbank:***
Die gemeinsame Sprache aller relantionalen Datenbanken ist ______ . Der Entwickler, der seine Daten in einer SQL-basierten Datenbank speichern möchte hat die Wahl zwischen mehreren Anbietern. Im Zusammenhang mit der Programmierung von Android Applikationen oder Electron Anwendungen ist ___________ eine gute Wahl, weil die eigentliche Datenbanksoftware leicht mit der Anwendung zusammen ausgeliefert werden kann. 
Der Name der Datenbank taucht im Quelltext mindestens an zwei Stellen auf. Das erste Mal wenn die Datenbank erstellt bzw von der Festplatte in den Arbeitsspeicher geladen wird.
Das zweite Mal wenn die Datenbank zurück auf die Festplatte geschrieben wird.
Jede Datenbank enthält im Regelfall eine Vielzahl von Tabellen. Der Name der einzelnen Tabelle entspricht oft dem Namen einer Klasse im Quelltext von JavaScript.
Jeder neue Zeile in einer Tabelle nennt man einen  _____________ .  
Jeder einzelne Datensatz repräsentiert ein einzelnes  _____________  der Klasse mit seinen individuellen _______________________  . 
Der einzelne Eigenschaftswert steht in einem  _____________ .















