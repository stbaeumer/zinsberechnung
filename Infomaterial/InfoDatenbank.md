# Info und Aufgabe Datenbank

Datenbanken kommen immer dann zu Einsatz, wenn Daten nach dem erneuten Starten eines Programms weiterhin zur Verfügung stehen sollen.

## Beispiele für den Einsatz von Datenbanken:

* Die Schulsekretärin legt einen neuen Schüler im Schulverwaltungsprogramm an. Für die Dauer des Schulbesuchs des Schülers werden seine Stammdaten gespeichert.
* Auf dem Handy werden nach jedem Spiel die Highscores gespeichert.
* Damit der Verlauf in WhatsApp erhalten bleibt, wird für jede neue Nachricht der Whatsapp-Datenbank ein neuer Datensatz hinzugefügt.

## SQL

Die Kommunikation des Javascript-Programms mit der Datenbank geschieht über die Datenbank-Sprache *SQL*. 
Die Anweisungen zum Einfügen, Auslesen usw. werden in SQL formuliert und dann als String an die Datenbank übergeben. 

> Beispielhafte Anweisung zum Anlegen einer Tabelle namens `autoTabelle` mit zwei Spalten:

> `CREATE TABLE autoTabelle (ps int, farbe char)` 

> Beispielhaftes Anlegen eines neuen Datensatzes. Die Eigenschaft `leistung` des Objekts `auto` wird hier in der SQL-Anweisung zu einem String verkettet. 
> Auf diese Weise kann zur Laufzeit der übergebene Eigenschaftswert variieren:

> `"INSERT INTO autoTabelle VALUES (" + auto.leistung + ", 'grün')`

## Begriffe

### Datenbank
Die Datenbank trägt einen Namen. In einer relationalen Datenbank werden alle Sachverhalte in Tabellen abgespeichert. Jeder Sachverhalt bekommt seine eigene(n) Tabelle(n). 

>In einem Autoverwaltungsprogramm gibt es eine Tabelle für Autos, eine für Fahrer, eine für Führerscheine usw.

### Tabelle
Jede Tabelle ist unter Ihrem eindeutigen Namen ansprechbar. Jede Spalte (Attribut) trägt einen Namen und kann nur Werte eines bestimmten Typs aufnehmen. 

> `int` entspricht dem Typ Ganzzahl. Beispiel *LeistungInPs*

> `char` steht für Zeichenketten. Beispiel *Farbe*

### Datensatz (Zeile)
Jede Zeile in einer Tabelle entspricht einem Datensatz. Ein Datensatz erfasst also alle Daten eines Objekts der realen Welt.

> In der Tabelle namens *autoTabelle* im Autoverwaltungsprogramm werden die Eigenschaftswerte der verschiedenen Autos mit ihren relevanten Eigenschaften erfasst.

### Datenfeld
Ein Datenfeld ist die Schnittmenge einer einzelnen Spalte (Attribut) mit einer einzelnen Zeile (Datensatz). Eine Datenzelle enthält einen einzelnen Wert.

> Die Farbe *grün* ist der Wert an der Schnittmenge der Spalte *Farbe* und der ersten *Zeile*

## Datenbank-Anbieter
Die Auswahl an Datenbank-Anbietern / -Herstellern ist groß. Obwohl alle relationalen Datenbanken grundsätzlich *SQL* sprechen, gibt es im Detail jedoch Unterschiede. 
Kriterien zur Auswahl des richtigen Anbieters könnten sein:

* Performance / Geschwindigkeit
* Kosten
* Datenschutz / Datensicherheit
* Mehrbenutzerfähigkeit

Im Falle einer besonders kleinen Umgebung (z.B. einer Electron-App) ist [SQLite](https://de.wikipedia.org/wiki/SQLite) eine gute Wahl. Der große Vorteil im Zusammenhang mit Javascript liegt darin, dass SQLite als reine Javascriptdatei in dieser ReferenzApp enthalten ist, ohne dass ein Programm installiert werden muss.

# Aufgabe Handyverwaltung

Für einen Handyshop sollen Sie eine Software zur Verwaltung von Smartphones erstellen. 

Beginnen Sie mit dem Entwurf einer einfachen Oberfläche zur Entgegennahme relevanter Eigenschaften. Alle relevanten Eigenschaften der realen Welt müssen zu Eigenschaften einer sinnvoll bezeichneten Klasse werden.

Die Benennung der Funktionen ist konsistent durchzuführen:

Die folgende btnClick-Funktion ...

`btnAutoEintragenOnClick()`

... muss zur Instanzmethode

`autoEintragen()`

führen.

Bei Klick auf den Button zum Hinzufügen eines neuen Handys muss automatisch direkt in die Datenbank geschrieben werden.

